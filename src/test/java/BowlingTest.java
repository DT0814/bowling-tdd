import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BowlingTest {
    private Bowling bowling;

    @BeforeEach
    void setUp() {
        bowling = new Bowling();
    }

    @Test
    void should_be_return_one_throw_score() {
        //Arrange
        //Act
        bowling.addThrow(5);
        //Assert
        assertEquals(5, bowling.getScores());
    }

    @Test
    void should_be_return_one_round_score() {
        //Arrange
        //Act
        bowling.addThrow(1);
        bowling.addThrow(1);
        //Assert
        assertEquals(2, bowling.getScores());
    }

    @Test
    void should_be_return_one_inning_score() {
        //Arrange

        //Act
        Stream.generate(() -> 1).limit(20).forEach(i -> bowling.addThrow(i));
        //Assert
        assertEquals(20, bowling.getScores());
    }

    @Test
    void should_be_throw_Ill_exception_when_param_Ill() {
        //Arrange

        //Act
        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            bowling.addThrow(-1);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            bowling.addThrow(11);
        });
    }

    @Test
    void should_be_return_one_inning_score_and_have_spare_no_last_round() {
        //Arrange
        List<Integer> list = Stream.of(5, 5).collect(Collectors.toList());
        Stream.generate(() -> 1).limit(18).forEach(i -> list.add(i));
        //Act
        list.forEach(i -> bowling.addThrow(i));
        //Assert
        assertEquals(29, bowling.getScores());
    }

    @Test
    void should_be_return_one_inning_score_and_have_spare_last_round() {
        //Arrange
        List<Integer> list = new ArrayList<>();
        Stream.generate(() -> 1).limit(18).forEach(list::add);
        list.add(5);
        list.add(5);
        list.add(1);
        //Act
        list.forEach(i -> bowling.addThrow(i));
        //Assert
        assertEquals(29, bowling.getScores());
    }

    @Test
    void should_be_return_one_inning_score_and_have_two_spare() {
        //Arrange
        List<Integer> list = Stream.of(5, 5).collect(Collectors.toList());
        Stream.generate(() -> 1).limit(16).forEach(list::add);
        list.add(5);
        list.add(5);
        list.add(1);
        //Act
        list.forEach(i -> bowling.addThrow(i));
        //Assert
        assertEquals(38, bowling.getScores());
    }

    @Test
    void should_be_return_one_inning_score_and_have_strike_no_last_round() {
        //Arrange
        List<Integer> list = Stream.of(10).collect(Collectors.toList());
        Stream.generate(() -> 1).limit(18).forEach(i -> list.add(i));
        //Act
        list.forEach(i -> bowling.addThrow(i));
        //Assert
        assertEquals(30, bowling.getScores());
    }

    @Test
    void should_be_return_one_inning_score_and_have_strike_last_round() {
        //Arrange
        List<Integer> list = new ArrayList<>();
        Stream.generate(() -> 1).limit(18).forEach(i -> list.add(i));
        list.add(10);
        list.add(1);
        list.add(1);
        //Act
        list.forEach(i -> bowling.addThrow(i));
        //Assert
        assertEquals(30, bowling.getScores());
    }

    @Test
    void should_be_return_one_inning_score_and_have_strike_and_spare() {
        //Arrange
        List<Integer> list = Stream.of(5, 5).collect(Collectors.toList());
        Stream.generate(() -> 1).limit(16).forEach(i -> list.add(i));
        list.add(10);
        list.add(1);
        list.add(1);
        //Act
        list.forEach(i -> bowling.addThrow(i));
        //Assert
        assertEquals(39, bowling.getScores());
    }

    @Test
    void should_be_return_one_inning_score_and_have_two_strike() {
        //Arrange
        List<Integer> list = Stream.of(10).collect(Collectors.toList());
        Stream.generate(() -> 1).limit(16).forEach(i -> list.add(i));
        list.add(10);
        list.add(1);
        list.add(1);
        //Act
        list.forEach(i -> bowling.addThrow(i));
        //Assert
        assertEquals(40, bowling.getScores());
    }
}
