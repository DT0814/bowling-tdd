import java.util.ArrayList;
import java.util.List;

public class Bowling {
    private List<Integer> scores = new ArrayList<>();
    private static int ROUND_START;

    public void addThrow(int throwScore) {
        if (throwScore > 10 || throwScore < 0) {
            throw new IllegalArgumentException();
        }
        scores.add(throwScore);
    }

    public int getScores() {
        Integer sum = 0;
        Integer maxScoreInRound = 10;
        int round = 0;
        ROUND_START = 2;
        for (int i = 0; round <= 10 && i < scores.size() && i < 20; i++) {
            if (checkRoundStart(i) && i + 1 < scores.size()) {
                if (maxScoreInRound.equals(scores.get(i))) {
                    changeRoundStart();
                    sum = getStrikeScore(sum, i);
                    if (++round == 10) {
                        break;
                    }
                    continue;
                }
                int roundScore = scores.get(i) + scores.get(i + 1);
                if (roundScore == maxScoreInRound && i + 2 < scores.size()) {
                    sum += scores.get(i + 2);
                }
                round++;
            }
            sum += scores.get(i);
        }
        return sum;
    }

    private Integer getStrikeScore(Integer sum, int index) {
        sum += scores.get(index);
        sum += scores.get(index + 1);
        sum += scores.get(index + 2);
        return sum;
    }

    private boolean checkRoundStart(int index) {
        boolean res = index % 2 == 0;
        if (ROUND_START == 2) {
            return res;
        }
        return !res;
    }

    private void changeRoundStart() {
        if (ROUND_START == 2) {
            ROUND_START = 1;
        } else {
            ROUND_START = 2;
        }
    }
}
